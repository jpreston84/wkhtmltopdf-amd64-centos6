wkhtmltopdf
================

This repository contains the binaries from the [wkhtmltopdf project](http://wkhtmltopdf.org/).
More about the functionality of wkhtmltopdf and wkthmltoimage, including full source code, can be found there.

## Getting the right version

This package contains binaries specifically compiled for 64-bit systems running CentOS 6. For other packages containing binaries for different versions, please check out [http://gitlab.com/jpreston84](http://gitlab.com/jpreston84).

## Installation

___Hint:___ The tags in this repository match the versions of the binaries they contain. For instance, git tag 0.12.2.1 contains binary version 0.12.2.1.

This package was designed to be installed with [Composer](https://getcomposer.org) through [Packagist](http://packagist.org). You may clone this repository with Git, and should be able to use the binary, but this use is not intended, and thus is unsupported.

Simply require this package with:

	composer require jpreston84/wkhtmltopdf-amd64-centos6 "0.12.2.1"

This will download the binary to your ___vendor/___ directory. It will also create a symlink to the binary at ___vendor/bin/wkhtmltopdf___. This symlink is the recommended path from which you should use the binary.

## Native Packages:

In case this package does _not_ work on your system, try installing the appropriate packages for your system directly from [http://wkhtmltopdf.org/downloads.html](http://wkhtmltopdf.org/downloads.html).
